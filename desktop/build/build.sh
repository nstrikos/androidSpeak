#rm -rf ~/$1
#mkdir ~/$1
#cd ~/$1
#cd ~
#git clone https://gitlab.com/nstrikos/$1.git
rm -rf ~/builds/$1
mkdir -p ~/builds/$1
cd ~/builds/$1
~/projects/Qt/6.7.3/gcc_64/bin/qmake ~/projects/gitlab/$1/desktop/src/$1.pro
make -j$3
cp $1 ~/packages/$1_$2_amd64/usr/share/$1/$1
cd ~/packages
#rm -rf ~/$1
#rm -rf ~/builds/$1
dpkg-deb --build --root-owner-group $1_$2_amd64
