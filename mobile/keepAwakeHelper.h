#ifndef KEEPAWAKEHELPER_H
#define KEEPAWAKEHELPER_H

#include <QJniObject>

class KeepAwakeHelper
{
public:
    KeepAwakeHelper();
    virtual ~KeepAwakeHelper();

private:
    QJniObject m_wakeLock;
};

#endif // KEEPAWAKEHELPER_H
